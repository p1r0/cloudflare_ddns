#!/bin/sh

set -o allexport
. ./.env
set +o allexport

# This helps to get the ID, ZONE and other Data 
curl -s -X GET "https://api.cloudflare.com/client/v4/zones/"$ZONE_ID"/dns_records" \
     -H	"Authorization: Bearer "$API_KEY\
     -H "Content-Type:application/json" | jq -r '.result[] | select(.name=="'$DOMAIN_NAME'")' 
