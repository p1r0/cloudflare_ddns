# cloudflare_DDNS

A Simple bash script that helps updating the DNS IP into cloudflare Works as DDNS running it by a crontab

## cloudflare_DDNS Tree

```sh.
.
├── cfDDNS.sh
├── LICENSE
├── README.md
└── zonesData.sh
```

## Requirements
* /bin/sh
* crontab
* jq

## Instalation

**Install jq (debian based systems)**
```shell
$ sudo apt install jq
```

**Clone the repo into /var/opt directory:**
```shell
$ cd /var/opt
$ git clone https://gitlab.com/p1r0/cloudflare_ddns.git
$ cd cloudflare_ddns
```

**Set enviroment variables**
```sh
$ cp envSample .env
```

***Add the variables values into the .env file***

```sh
$ nano .env
ID="YOUR_ID"
ZONE_ID="YOUR_ZONE_ID"
API_KEY="YOUR_API_KEY"
DNS_TYPE="A"
DOMAIN_NAME="your.website.com"
```

> NOTE:
> If you do not know your zoneID, ID and others, just set the APIKEY into .env and run the ./zonesData.sh


## Setup the crontab for DDNS

To resolve and update the UP ADDRESS this script should be configured in a crontab to run every 5 minutes. The crontab should be configured as the user that holds this repository and that runs the script.

*Open the crontab as the user that holds the script*
```sh
 $ crontab -e
```
*Append the following line at the end of the editor and write it*

```sh
0,5,10,15,20,25,30,35,40,45,50,55 * * * * sleep 28 ; /var/opt/cloudflare_ddns/cfDDNS.sh
```
**NOTE: the crontab must have execution permisions into the cfDDNS.sh by the user that is going to run it**

*Finally verify the logs at /tmp/DDNS.log to verify that it is working:*


```sh
$ cat /tmp/DDNS.log
```

## License

[**UNLICENSE**](./LICENSE)

## Information and contacts.

***Developer***

- David E. Perez Negron [david@neetsec.com](mailto:david@neetsec.com)

## ToDo

 - extract the data from zones and id automatically
 - set run options as first and as crontab
